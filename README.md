# LibreWolf for Archlinux
- [project website](https://librewolf.net/): read some docs and discover the project;
- [faq](https://librewolf.net/docs/faq/): most of your issues might already be solved there;
- [issue tracker](https://gitlab.com/librewolf-community/browser/arch/-/issues);
- [source repository](https://gitlab.com/librewolf-community/browser/source);
- [settings repository](https://gitlab.com/librewolf-community/settings);
- join us on [gitter](https://gitter.im/librewolf-community/librewolf) / [matrix](https://matrix.to/#/#librewolf:matrix.org) / [lemmy](https://lemmy.ml/c/LibreWolf/) / [reddit](https://www.reddit.com/r/LibreWolf/)

# Installation
### AUR Helper:
- [paru](https://github.com/Morganamilo/paru): install using paru `paru -S brave`
- [yay](https://github.com/Jguer/yay): install using yay `yay -S brave`
### Or Manually:
- `
git clone https://aur.archlinux.org/brave.git` then `cd brave` and `makepkg -si`
- you need to install the base-devel package at first if you did not have installed.
`sudo pacman -S base-devel`

## Note
there is three versions of brave on AUR - **brave, brave-bin, brave-git** you can install whatever version you want, <br> the installation process will be the same.

## License
[Mozilla Public License Version 2.0](https://gitlab.com/librewolf-community/browser/linux/-/blob/master/LICENSE.txt)
